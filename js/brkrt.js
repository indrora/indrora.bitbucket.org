/**
 * brkrt.js
 * Hub.me.js for Bitbucket.
 */
 
 ;(function ( $, window, document, undefined ) {
    var brkrt = 'brkrt',
    defaults = { "username":"indrora","exclude":[],"itemclass":"repo" };
    
    function Plugin(element, options)
    {
      this.element = element;
      this.options = $.extend( {}, defaults, options);
      this.getRepos();
    }
    
    Plugin.prototype.getRepos = function ()
    {
      var self = this, thisRepo = self.options.username + ".bitbucket.org";
      var repos = [];
      
      $.getJSON('https://api.bitbucket.org/1.0/users/'+self.options.username+'/?callback=?',
        function(result) {
          $.each(result.repositories, function(i, field) {            
           	if ( $.inArray(field.name, self.options.exclude) === -1 && field.language != null && field.name != thisRepo  )
              repos.push(field);
            }); // $.each->anon_function
        
           $.each(repos, function(i, repo) {
           var repo_url = '//bitbucket.org/'+self.options.username+'/'+repo.slug+'/';
            var repoblock = '<div class="'+self.options.itemclass+'"><article>'+
                            '<h2><a href="'+repo_url+'">'+repo.name+'</a></h2>'+
                            '<p>'+repo.description+'</p>'+
                            '</article></div>'
            $(self.element).append(repoblock);
           }); // $.each->anon-function
        } // anon-founction ($.getJSON->option)
      
      ); // $.getJSON
    }
    
    // Mildly stolen from Hub.me.js
    
    $.fn.brkrt = function(options) {
     return this.each(function () {
      if (!$.data(this, 'plugin_' + brkrt)) {
       $.data(this, 'plugin_' + brkrt, new Plugin( this, options ));
      }
     });
    };
    
 })(jQuery, window, document)